import { Component, OnInit } from '@angular/core';

import { AuthService } from './components/auth/auth.service';
import { LoginRsp } from './components/auth/auth.model';
import { Role } from './shared/models/userRole.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private currentUser: LoginRsp | null;
  constructor(
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(x => (this.currentUser = x));
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.userType === Role.Admin;
  }

  ngOnInit() {}

  logout() {
    this.authService.logout();
  }
}
