import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes = [LoginComponent, NotFoundComponent];

@NgModule({
  declarations: [...routes],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [...routes]
})
export class ComponentsModule { }
