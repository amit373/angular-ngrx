import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from './../../../store/index';
import * as authActions from './../../../store/actions/auth.action';
import { AuthService } from 'src/app/components/auth/auth.service';
import { LoginVM } from 'src/app/components/auth/auth.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  returnUrl: string;
  error: string = '';
  loginObj: LoginVM;
  subscription$: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private store: Store<AppState>,
  ) {
    this.loginObj = new LoginVM();
    // redirect to home if already logged in
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    const dataObj: LoginVM = {
      email: this.loginObj.email,
      password: this.loginObj.password
    };
    this.store.dispatch(new authActions.LoginUser(dataObj));
    this.subscription$ = this.store
      .select((state: any) => state.authReducer)
      .subscribe((res?: any) => {
        this.loading = res.loading;
        if (res.user && res.loaded) {
          const { userType } = res.user;
          if (userType === 'admin') {
            this.router.navigateByUrl('/admin/dashboard');
          } else if (userType === 'user') {
            this.router.navigateByUrl('/users/dashboard');
          }
        }
      });
  }
}
