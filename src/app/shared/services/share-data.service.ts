import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as _ from 'lodash';

import { StorageService } from './storage.service';

@Injectable()
export class ShareDataService {
  emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(private storageService: StorageService) { }

  static toFixedDown(integer: number, digits: number = 0) {
    const numberString = (integer || 0).toFixed(10);
    const re = new RegExp('(\\d+\\.\\d{' + digits + '})(\\d)'), m = numberString.match(re);
    const result = m ? parseFloat(m[1]) : parseFloat(numberString).valueOf();
    return integer >= 0 ? result : (-1 * result);
  }

  static countDecimals(value: any) {
    const match = ('' + Number(value)).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    if (!match) {
      return 0;
    }
    return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
  }

  onKeyPressAllowNumbers(e: any, val: any) {
    if ([46, 8, 9, 27, 13, 110].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      if (e.keyCode === 46 && val.indexOf('.') === -1) {
        return;
      }
    }
    // Ensure that it is a number and stop the keypress & do not allow multiple points
    if (e.keyCode < 48 || e.keyCode > 57) {
      e.preventDefault();
    }
  }

  getErrorMessage(err: any) {
    let msg = 'Something Went Wrong';

    if (err.status === 401) {
      console.warn('DB cleared after 401 error');
      this.storageService.resetStorage();
      return msg;
    }

    if (err.status === 500) {
      return msg;
    }

    if (err.error) {
      delete err.error.status;
      if (err.error.message) {
        msg = err.error.message;
      } else if (_.isArray(err.error[Object.keys(err.error)[0]])) {
        msg = err.error[Object.keys(err.error)[0]][0];
      } else {
        msg = err.error[Object.keys(err.error)[0]];
      }
    } else if (err.message) {
      msg = err.message;
    } else if (_.isArray(err[Object.keys(err)[0]])) {
      msg = err[Object.keys(err)[0]][0];
    } else if (err.data && typeof err.data === 'string') {
      msg = err.data;
    } else if (err[Object.keys(err)[0]]) {
      msg = err[Object.keys(err)[0]];
    }

    if (msg === 'true' || typeof msg !== 'string') {
      if (err.status && err.status >= 200 && err.status < 300) {
        msg = 'success';
      } else {
        msg = 'Something Went Wrong';
      }
    }

    return msg;
  }

  exctractApiError(resError: HttpErrorResponse): any {
    let errors = [{ title: 'Error!', detail: 'Ooops, something went wrong!' }];
    if (resError && resError.error && resError.error.errors) {
      errors = resError.error.errors;
    }
    return errors;
  };


  cleanUrl(url: string): string {
    // remove first route (demo name) from url router
    if (new RegExp(/^\/demo/).test(url)) {
      const urls = url.split('/');
      urls.splice(0, 2);
      url = urls.join('/');
    }
    if (url.charAt(0) == '/') {
      url = url.substr(1);
    }
    // we get the page title from config, using url path.
    // we need to remove query from url ?id=1 before we use the path to search in array config.
    let finalUrl = url.replace(/\//g, '.');
    if (finalUrl.indexOf('?') !== -1) {
      finalUrl = finalUrl.substring(0, finalUrl.indexOf('?'));
    }
    return finalUrl;
  }

  isEmail(email: string): boolean {
    return this.emailPattern.test(String(email).toLowerCase());
  }
}
