import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() {}

  saveToLocalStorage = (key: any, value: any) => {
    localStorage[key] = JSON.stringify(value);
  }

  getFromLocalStorage = (key: any): any => {
    if (localStorage[key]) {
      return JSON.parse(localStorage[key]);
    } else {
      return false;
    }
  }

  removeFromLocalStorage = (key: any): any => {
    localStorage.removeItem(key);
  }

  saveToSession = (key: any, value: any) => {
    sessionStorage[key] = JSON.stringify(value);
  }

  getSessionStorage = (key: any): any => {
    if (sessionStorage[key]) {
      return JSON.parse(sessionStorage[key]);
    } else {
      return false;
    }
  }

  removeFromSessionStorage = (key: any): any => {
    sessionStorage.removeItem(key);
  }

  resetStorage(): any {
    localStorage.clear();
    sessionStorage.clear();
  }
}
