import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './storage.service';
export interface Locale {
    lang: string;
    data: Object;
}
@Injectable({
    providedIn: 'root'
})
export class TranslationService {
    private langIds: any = [];
    constructor(
        private translateService: TranslateService,
        private storageService: StorageService
    ) {
        this.translateService.addLangs(['en']);
        this.translateService.setDefaultLang('en');
    }

    // Load Translation
    loadTranslations(...args: Locale[]): void {
        const locales = [...args];
        locales.forEach(locale => {
            this.translateService.setTranslation(locale.lang, locale.data, true);
            this.langIds.push(locale.lang);
        });
        this.translateService.addLangs(this.langIds);
    }

    //  Setup language
    setLanguage(lang: string) {
        if (lang) {
            this.translateService.use(this.translateService.getDefaultLang());
            this.translateService.use(lang);
            this.storageService.saveToLocalStorage('language', lang)
        }
    }

    // Returns selected language
    getSelectedLanguage(): any {
        return this.storageService.getFromLocalStorage('language') || this.translateService.getDefaultLang();
    }
}
