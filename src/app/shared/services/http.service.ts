import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(public httpClient: HttpClient) { }

  setHeaders() {
    const headers = new HttpHeaders();
    return headers;
  }

  getHTTPHeaders(): HttpHeaders {
    const result = new HttpHeaders();
    result.set('Content-Type', 'application/json');
    return result;
  }

  get(endpoint: string) {
    return this.httpClient.get<any>(`${environment.apiUrl}/${endpoint}`);
  }

  post(endpoint: string, data: any) {
    return this.httpClient.post<any>(`${environment.apiUrl}/${endpoint}`, data);
  }

  put(endpoint: string, data: any) {
    return this.httpClient.put<any>(`${environment.apiUrl}/${endpoint}`, data);
  }

  patch(endpoint: string, data: any) {
    return this.httpClient.patch<any>(`${environment.apiUrl}/${endpoint}`, data);
  }

  delete(endpoint: string, id: number) {
    return this.httpClient.delete(`${environment.apiUrl}/${endpoint}/${id}`);
  }
}
