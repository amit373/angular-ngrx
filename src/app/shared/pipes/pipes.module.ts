import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UppercasePipe } from './uppercase.pipe';
import { SearchPipe } from './search.pipe';
import { TimeFormatPipe } from './time-format.pipe';
import { GetObjectPipe } from './get-object.pipe';
import { JoinPipe } from './join.pipe';
import { SafePipe } from './safe.pipe';
import { TimeElapsedPipe } from './time-elapsed.pipe';
import { SortByPipe } from './sortBy.pipe';

const pipes = [
    UppercasePipe,
    TimeFormatPipe,
    GetObjectPipe,
    JoinPipe,
    SafePipe,
    TimeElapsedPipe,
    TimeFormatPipe,
    SearchPipe,
    SortByPipe
];
@NgModule({
    declarations: [...pipes],
    exports: [...pipes],
    imports: [CommonModule]
})
export class PipesModule { }
