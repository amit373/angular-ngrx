import { Action } from '@ngrx/store';

export enum UserActionsTypes {
  LOAD_USERS = '[User] Load users',
  LOAD_USERS_SUCCESS = '[User] Load users success',

  LOAD_USER = '[User] Load user',
  LOAD_USER_SUCCESS = '[User] Load user success'
}

export class LoadUsers implements Action {
  readonly type = UserActionsTypes.LOAD_USERS;
}

export class LoadUsersSuccess implements Action {
  readonly type = UserActionsTypes.LOAD_USERS_SUCCESS;
  constructor(public payload: any) {}
}

export class LoadUser implements Action {
  readonly type = UserActionsTypes.LOAD_USER;
  constructor(public payload: string) {}
}

export class LoadUserSuccess implements Action {
  readonly type = UserActionsTypes.LOAD_USER_SUCCESS;
  constructor(public payload: any) {}
}

export type UserActions = LoadUsers | LoadUsersSuccess | LoadUser | LoadUserSuccess;