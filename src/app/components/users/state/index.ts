import * as Store from '../../../store';

export interface UserState {
  loading: boolean;
  loaded: boolean;
  users: any;
}

export interface AppState extends Store.AppState {
  users: UserState;
}
