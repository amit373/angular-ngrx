import { UserState } from '.';
import { UserActions, UserActionsTypes } from './user.action';

const initialState: UserState = {
  loading: false,
  loaded: false,
  users: []
};

export const userReducer: (state: UserState, action: UserActions) =>
    UserState = (state: UserState = initialState, action: UserActions) => {
  switch (action.type) {
    case UserActionsTypes.LOAD_USERS: {
      return { ...state, loading: true, loaded: false };
    }
    case UserActionsTypes.LOAD_USER: {
      return { ...state, loading: true, loaded: false };
    }
    case UserActionsTypes.LOAD_USERS_SUCCESS: {
      const users = action.payload;
      return { ...state, users, loading: false, loaded: true };
    }
    default:
      return state;
  }
};
