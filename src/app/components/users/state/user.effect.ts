import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap, catchError, mergeMap, map } from 'rxjs/operators';

import { AppState } from '.';
import * as fromUser from './user.action';
import * as fromError from './../../../store/actions/error.action';
import { UsersService } from '../users.service';

@Injectable()
export class UserEffects {
  constructor(
    private action$: Actions,
    private store: Store<AppState>,
    private usersService: UsersService
  ) {}

  @Effect()
  loadUsers$: Observable<Action> = this.action$.pipe(
    ofType<fromUser.LoadUsers>(fromUser.UserActionsTypes.LOAD_USERS),
    tap(() => this.store.dispatch(new fromError.RemoveError())),
    mergeMap(() =>
      this.usersService.getUsers().pipe(
        map(users => new fromUser.LoadUsersSuccess(users)),
        catchError(err => of(new fromError.AddError(err.error)))
      )
    )
  );
}
