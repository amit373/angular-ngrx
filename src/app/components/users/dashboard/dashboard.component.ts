import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from './../../../store';
import * as usersActions from './../state/user.action';
import * as usersSeclector from './../state/user.selector';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  users: Observable<any>;
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.dispatch(new usersActions.LoadUsers());
    this.users = this.store.select(usersSeclector.selectUsers);
  }

}
