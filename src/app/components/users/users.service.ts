import { HttpService } from 'src/app/shared/services/http.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class UsersService {
    constructor(private httpService: HttpService) {}
    getUsers(): Observable<any> {
       return this.httpService.get('');
    }
}
