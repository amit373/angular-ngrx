import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserEffects } from './state/user.effect';
import { userReducer } from './state/user.reducer';

@NgModule({
  declarations: [UsersComponent, DashboardComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    StoreModule.forFeature('userReducer', userReducer),
    EffectsModule.forFeature([UserEffects])
  ]
})
export class UsersModule { }
