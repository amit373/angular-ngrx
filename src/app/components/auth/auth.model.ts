export class LoginVM {
  email: string;
  password: string;
}

export interface LoginRsp {
  type: any;
  success?: boolean;
  token?: string;
  name?: string;
  email?: string;
  userType?: string;
  iat?: number;
  exp?: number;
}
