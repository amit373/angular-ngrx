import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as moment from 'moment';

import { StorageService } from './../../shared/services/storage.service';
import { HttpService } from 'src/app/shared/services/http.service';
import { LoginRsp, LoginVM } from './auth.model';
import { Router } from '@angular/router';
import { AppState } from './../../store';
import { ResetState } from './../../store/actions/auth.action';
// '16:36' < '18:36' - not expired!
// '16:36' < '15:36' - expired!
@Injectable()
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    private storageService: StorageService,
    private httpService: HttpService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(
      this.storageService.getFromLocalStorage('currentUser')
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  setToken(token: string): string | any {
    this.storageService.saveToLocalStorage('authToken', token);
  }

  getToken(): void {
    this.storageService.getFromLocalStorage('authToken');
  }

  login(data: LoginVM) {
    return this.httpService.post('auth/login', data).pipe(
      map((user?: LoginRsp) => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.storageService.saveToLocalStorage('currentUser', user);
          this.currentUserSubject.next(user);
        }
        return user;
      })
    );
  }

  logout(): any {
    this.store.dispatch(new ResetState());
    this.currentUserSubject.next(null);
  }

  isAuthorized(allowedRoles: string[]): boolean {
    // check if the list of allowed roles is empty, if empty, authorize the user to access the page
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }

    // get token from local storage or state management
    const currentUser = this.storageService.getFromLocalStorage('currentUser');

    // check if it was decoded successfully, if not the token is not valid, deny access
    if (!currentUser.token) {
      return false;
    }

    // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
    return allowedRoles.includes(currentUser.userType);
  }

  checkAuthentication(): boolean {
    const authToken = this.storageService.getFromLocalStorage('currentUser');
    if (!authToken) { return false; }
    return true;
  }

  get getCurrentUser(): string {
    return this.storageService.getFromLocalStorage('currentUser') || '';
  }

  get isAuthenticated(): boolean {
    return moment().isBefore(this.expiration);
  }

  private get expiration() {
    const expirationTime = this.storageService.getFromLocalStorage('currentUser').exp
    return moment.unix(expirationTime);
  }
}
