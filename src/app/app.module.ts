import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppStoreModule } from './store/app-store.module';
// Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Modules
import { ComponentsModule } from './shared/components/components.module';
// Services
import { HttpService } from './shared/services/http.service';
import { TokenInterceptor } from './shared/interceptor/token.interceptor';
import { AuthService } from './components/auth/auth.service';
import { StorageService } from './shared/services/storage.service';
import { ErrorInterceptor } from './shared/interceptor/error.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgxSpinnerModule,
    AppStoreModule
  ],
  providers: [
    HttpService,
    AuthService,
    StorageService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
