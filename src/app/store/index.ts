import { ActionReducerMap, MetaReducer, ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { storeLogger } from 'ngrx-store-logger';
import * as fromRouter from '@ngrx/router-store';

import { environment } from 'src/environments/environment';
// Effects
import { AuthEffects } from './effects/auth.effect';
// Reducers
import { authReducer, AuthState } from './reducers/auth.reducer';
import { RouterStateUrl } from './reducers/router.reducer';

export const effects = [AuthEffects];

export const reducers: ActionReducerMap<AppState> = {
  authReducer,
  router: fromRouter.routerReducer
};

export interface AppState {
  authReducer: AuthState;
  router: fromRouter.RouterReducerState<RouterStateUrl>;
}

export function logger(reducer: ActionReducer<AppState>): any {
  return storeLogger()(reducer);
}

export const metaReducers: Array<MetaReducer<AppState>> = !environment.production ? [storeFreeze, logger] : [];