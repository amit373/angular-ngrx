import { Injectable } from '@angular/core';
import { Actions as EffectActions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { mergeMap, catchError, map, tap, switchMap } from 'rxjs/operators';

import { LoginRsp } from 'src/app/components/auth/auth.model';
import { StorageService } from './../../shared/services/storage.service';
import { AppState } from '..';
import { AuthService } from './../../components/auth/auth.service';
import * as authActions from './../actions/auth.action';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
  constructor(
    private effectActions$: EffectActions,
    private store: Store<AppState>,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router
  ) {}

  @Effect()
  LogIn: Observable<any> = this.effectActions$.pipe(
    ofType(authActions.AuthActionTypes.LOGIN_USER),
    map((action: authActions.LoginUser) => action.payload),
    switchMap(payload => {
      return this.authService.login(payload).pipe(
        map((data: LoginRsp) => {
          if (data.token) {
            return new authActions.SetCurrentUser(data);
          } else {
            return this.store.dispatch(new authActions.LogInFailure({}));
          }
        }),
        catchError(err => {
          return of(new authActions.LogInFailure({ err }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.effectActions$.pipe(
    ofType(authActions.AuthActionTypes.LOGIN_SUCCESS)
  );

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.effectActions$.pipe(
    ofType(authActions.AuthActionTypes.LOGIN_FAILURE)
  );

  @Effect({ dispatch: false })
  ResetState: Observable<any> = this.effectActions$.pipe(
    ofType(authActions.AuthActionTypes.RESET_STATE),
    map(() => {
      this.storageService.resetStorage();
      this.router.navigate(['login']);
    })
  );
}
