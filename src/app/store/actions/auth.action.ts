import { Action } from '@ngrx/store';
import { LoginVM, LoginRsp } from 'src/app/components/auth/auth.model';

export enum AuthActionTypes {
  LOGIN_USER = '[AUTH] Login user',
  LOGIN_SUCCESS = '[AUTH] Set current user',
  LOGIN_FAILURE = '[Auth] Login Failure',
  SET_INITIAL_USER = '[AUTH] Set initial user',
  RESET_STATE = '[Auth] Reset State'
}

export class LoginUser implements Action {
  readonly type = AuthActionTypes.LOGIN_USER;
  constructor(public payload: LoginVM) {}
}

export class SetInitialUser implements Action {
  readonly type = AuthActionTypes.SET_INITIAL_USER;
}

export class SetCurrentUser implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: LoginRsp | null) {}
}

export class LogInFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: any) {}
}

export class ResetState implements Action {
  readonly type = AuthActionTypes.RESET_STATE;
  constructor() {}
}

export type AuthActions = LoginUser | SetCurrentUser | SetInitialUser | LogInFailure | ResetState;
