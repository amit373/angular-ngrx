import { ErrorActions, ErrorActionsTypes } from '../actions/error.action';

export interface ErrorState {
  error: any;
}

const initialState: ErrorState = {
  error: null
};

export const errorReducer: (state: ErrorState, action: ErrorActions) =>
    ErrorState = (state = initialState, action: ErrorActions) => {
  switch (action.type) {
    case ErrorActionsTypes.ADD_ERROR:
      return { ...state, error: action.payload };
    case ErrorActionsTypes.REMOVE_ERROR:
      return { ...state, error: null };
    default:
      return state;
  }
};
