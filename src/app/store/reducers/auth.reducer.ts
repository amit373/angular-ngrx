import { AuthActionTypes, AuthActions } from './../actions/auth.action';
import { LoginRsp } from './../../components/auth/auth.model';

export interface AuthState {
  user: LoginRsp | any;
  loading: boolean;
  loaded: boolean;
  isAuthenticated: boolean;
  errorMessage: string;
}

const initialState: AuthState = {
  user: null,
  loading: false,
  loaded: false,
  isAuthenticated: false,
  errorMessage: null
};

export const authReducer: (state: AuthState, action: AuthActions) => AuthState = (
  state = initialState,
  action: AuthActions
) => {
  switch (action.type) {
    case AuthActionTypes.LOGIN_USER:
      return {
        ...state,
        loading: true,
        loaded: false,
        isAuthenticated: true,
        user: action.payload,
        errorMessage: null
      };
    case AuthActionTypes.SET_INITIAL_USER:
      return {
        ...state,
        loading: true,
        loaded: false,
        isAuthenticated: false,
        user: null,
        errorMessage: null
      };
    case AuthActionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        isAuthenticated: true,
        user: action.payload,
        errorMessage: null
      };
    }
    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: 'Incorrect email and/or password.'
      };
    }
    case AuthActionTypes.RESET_STATE: {
      return {
        ...state,
        loading: false,
        loaded: true,
        isAuthenticated: false,
        user: null,
        errorMessage: null
      };
    }
    default:
      return state;
  }
};
